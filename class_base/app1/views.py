# from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
# Create your views here.


class HomeView(View):

    def get(self,request):

        return HttpResponse("<p> This is class based view</p>")

    def post(self,request):

        return HttpResponse(" this post class based view")


class GreetingView(View):
    greeting = "hello user"

    def get(self,request):

        return HttpResponse(self.greeting)


class Greet(GreetingView):
    greeting = "Welcome to greet view"

